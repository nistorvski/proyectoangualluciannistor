import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { URL } from '../../key/urls';
import { Recipe } from 'src/app/models/Irecipe';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private urlList = URL.urlRecipe;
  //private urlDetail = URL.urlDetail;

  public filtroRecipe="";

  constructor(private http:HttpClient) { }

  public getRecipe():Observable<Recipe[]> {
    return this.http.get(this.urlList).pipe(
      map((response: Recipe[]) => {
        if(!response) {
          throw new Error ('Value expected!');

        }else {
          return response;
        }
      }),
      catchError((err)=> {
        throw new Error(err.message);
      })
    );
  }

  public getDetail(id:number):Observable<Recipe> {
    return this.http.get(`${this.urlList}/${id}`).pipe(map((response: Recipe) => {
      if(!response)
      {
        throw new Error ('Value expected');
      }else {


        return response;

      }
    }),
    catchError((err)=> {
      throw new Error(err.message);
    })
    )
  }


  public postRecipe(recipe: Recipe): Observable<any> {
    return this.http.post(this.urlList,recipe).pipe(map((response: any) => {
      if(!response)
      {
        throw new Error ('Value expected');
      }else {


        return response;

      }
    }),
    catchError((err)=> {
      throw new Error(err.message);
    })
    )
  }

  public deleteRecipe(id: number):Observable<any> {
    return this.http.delete(`${this.urlList}/${id}`).pipe(map((response:any) => {
      if(!response)
      {
        throw new Error ('Value expected');
      }else {


        return response;

      }
    }),
      catchError((err)=> {
        throw new Error(err.message);
      })
    )
  }

}
