import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './homeComponent/home.component';
import { HomeBannerComponent } from './homeComponent/home-banner/home-banner.component';
import { HomeSectionComponent } from './homeComponent/home-section/home-section.component';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [HomeComponent, HomeBannerComponent, HomeSectionComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatFormFieldModule,
    MatInputModule,

  ]
})
export class HomeModule { }
