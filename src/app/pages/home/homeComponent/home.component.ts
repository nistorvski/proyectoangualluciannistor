import { Component, OnInit } from '@angular/core';
import { Title } from './model/iHome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public title: Title = {
    image:'https://png.pngtree.com/thumb_back/fw800/back_our/20190620/ourmid/pngtree-kitchen-gourmet-seasoning-taobao-e-commerce-banner-background-image_162746.jpg',
    title:'Elige comer en casa',
    subTitle:'¿Que te apetece comer hoy?',
    content:'Gran diversidad de recetas '
    
  }
  constructor() { }

  ngOnInit(): void {
  }

}
