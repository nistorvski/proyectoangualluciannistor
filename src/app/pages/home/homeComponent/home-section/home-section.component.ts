import { Component, Input, OnInit } from '@angular/core';
import { Title } from '../model/iHome';

@Component({
  selector: 'app-home-section',
  templateUrl: './home-section.component.html',
  styleUrls: ['./home-section.component.scss']
})
export class HomeSectionComponent implements OnInit {
@Input() title:Title;
  
  constructor() { }

  ngOnInit(): void {
  }

}
