import { Component, Input, OnInit } from '@angular/core';
import { Title } from '../model/iHome';


@Component({
  selector: 'app-home-banner',
  templateUrl: './home-banner.component.html',
  styleUrls: ['./home-banner.component.scss']
})
export class HomeBannerComponent implements OnInit {
 @Input() title:Title;
 
  constructor() { }

  ngOnInit(): void {
  }

}
