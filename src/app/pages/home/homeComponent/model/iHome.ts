export interface Title {
    image:string,
    title:string,
    subTitle:string,
    content:string,
}