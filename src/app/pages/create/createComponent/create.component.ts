import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Recipe } from 'src/app/models/Irecipe';
import { RecipeService } from 'src/app/services/recipe-service/recipe.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
public recipeForm: FormGroup | null = null; 
public submitted: boolean = false;
@Input() public showModal: boolean = false;

  constructor(private formBuilder:FormBuilder, private recipeService:RecipeService ) { }

  ngOnInit(): void {
    this.createRecipeForm();
  }


  public createRecipeForm(): void {
    this.recipeForm = this.formBuilder.group({
      id: ['', [Validators.required]],
      title: ['', [Validators.required]],
      ingredients: ['', [Validators.required]],
      elaboration: ['', [Validators.required]],
      image: ['', [Validators.required]],
    });
  }

  public onSubmit():void {
    this.submitted=true; 
    this.showModal = true;
    if(this.recipeForm.valid){
      this.postRecipe();
      this.recipeForm.reset();
      this.submitted = false;
    }
  }

  public postRecipe():void {
    const recipe : Recipe = {
        id:this.recipeForm.get('id').value,
        title:this.recipeForm.get('title').value,
        ingredients:this.recipeForm.get('ingredients').value,
        elaboration:this.recipeForm.get('elaboration').value,
        image:this.recipeForm.get('image').value,
    }
    this.recipeService.postRecipe(recipe).subscribe((data:Recipe) => {
      console.log(data);
    }),
    (err) => {
      console.error(err.message);
    }
  }


}
