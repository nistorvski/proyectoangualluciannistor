import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal-create',
  templateUrl: './modal-create.component.html',
  styleUrls: ['./modal-create.component.scss']
})
export class ModalCreateComponent implements OnInit {
  @Input() public showModal: boolean = false;
  @Output() private showModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  public getShowErrorChange(): any {
    return this.showModalChange;
  }

  public closeModal(): void {
    this.showModalChange.emit(!this.showModal);
  }

  public getShowModal(): any {  
 
    return this.showModalChange;
  }

  public closeShowModal(): void {
    this.showModalChange.emit(!this.showModal);
  }

  ngOnInit(): void {
        console.log(this.showModal);
  }

}
