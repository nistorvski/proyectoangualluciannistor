import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './createComponent/create.component';
import { ModalCreateComponent } from './createComponent/modal-create/modal-create.component';


@NgModule({
  declarations: [CreateComponent, ModalCreateComponent],
  imports: [
    CommonModule,
    CreateRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class CreateModule { }
