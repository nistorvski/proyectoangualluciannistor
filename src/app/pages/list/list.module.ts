import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './listComponent/list.component';
import { ItemListComponent } from './listComponent/item-list/item-list.component';
import { SearchFormComponent } from './listComponent/search-form/search-form.component';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { SearchPipe } from '../list/listComponent/pipe/search.pipe';


@NgModule({
  declarations: [ListComponent,
     ItemListComponent,
     SearchFormComponent,
    SearchPipe,
  ],
  imports: [
    CommonModule,
    ListRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,


  ]
})
export class ListModule { }
