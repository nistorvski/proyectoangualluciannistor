import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/Irecipe';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {
@Input() item:Recipe;
  constructor() { }

  ngOnInit(): void {
  }

}
