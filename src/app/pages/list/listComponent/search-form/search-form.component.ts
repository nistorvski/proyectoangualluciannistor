import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  @Output('search') searchEmitter = new EventEmitter<string>();

  public search = new FormControl('');

  constructor() { }

  ngOnInit(): void {
    this.onSubmit();
  }

  public onSubmit():void{
    this.search.valueChanges.pipe(debounceTime(300)).subscribe(value => this.searchEmitter.emit(value));

  }

}
