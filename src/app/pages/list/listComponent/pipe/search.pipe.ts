import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(list: any[], texto:string): any[] {
   if(!texto) return list;

   return list.filter(itemList => itemList.title.toUpperCase().includes(texto.toLocaleUpperCase()));

  }

}
