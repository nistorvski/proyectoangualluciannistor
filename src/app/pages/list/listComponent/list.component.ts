import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/Irecipe';
import { Location } from '@angular/common';
import { RecipeService } from 'src/app/services/recipe-service/recipe.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public itemList: Recipe[];
  public filtro_valor='';
  constructor(private recipeService: RecipeService, private location: Location) { }

  ngOnInit(): void {
    this.getItemList();
  }

  public getItemList():void {
    this.recipeService.getRecipe().subscribe(
      (data:Recipe[])=> {
        console.log(data);
        this.itemList = data;
      },
      (err) => {
        console.error(err.message);
      }
      )
  }

  public goBack():void {
    this.location.back();
  }


  public handleSearch(value:string):void {
    console.log(value);
    this.filtro_valor=value;
  }


}
