import { Component, OnInit } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Location } from '@angular/common';
import { RecipeService } from 'src/app/services/recipe-service/recipe.service';
import { Recipe } from '../../../models/Irecipe';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  public detail:Recipe;
  private itemId:string;
  public id;

  constructor(private recipeService: RecipeService, private location: Location, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getRecipeDetail();
    this.deleteRecipe(this.detail.id);

  }

  public getRecipeDetail():void {


    this.route.paramMap.subscribe((params) => {
      this.itemId = params.get('id'); 

      console.log( this.itemId);
      
    });

    this.recipeService.getDetail(Number(this.itemId)).subscribe((data: Recipe) => {
 
      console.log(data);
      this.detail=data;
  
    },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public deleteRecipe(id = this.detail.id):void {
    
    this.recipeService.deleteRecipe(id).subscribe((data)=> {

      data;
      this.location.back();

    },
    (err) => {
      console.error(err.message);
    }
  );
  }

  public goBack():void {
    this.location.back();
  }


}
